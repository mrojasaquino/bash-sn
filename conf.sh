#!/bin/bash
# Global configuration file.

# Define paths for the system

# Base directory for the Social Network utility
export SN_HOME=/home/mrojas/Development/Projects/Bash/SocialNetwork

# Central Social Network server
export CENTRAL_REPO_PATH=/home/mrojas/git/SocialNetworkBase
export SN_GIT_USER=mrojas
export SN_GIT_HOST=localhost

# Git User Name. Besides git, we're using it for the posts
export USER_NAME="Miguel Angel Rojas Aquino"
# Git User email
export USER_EMAIL="mrojasaquino@gmail.com"

# Base path for the social networks
export NETWORKS_HOME="${HOME}/SN"



## Normally, you should not touch from here onwards

# Tools required to be already installed
export REQUIREMENTS="${SN_HOME}/conf/requirements.txt"
# Paths for storing data
export REQUIRED_PATHS="${SN_HOME}/conf/network_paths.txt"
# Log file
export LOG_FILE="${SN_HOME}/sn_$(date +%Y-%m-%d).log"