#!/bin/bash

# Pre Flight Check
# Verify that all the required tools are already installed
function check_requirements() {
    local __RESULT=$1
    local __LOCAL_EXIT_STATUS=0

    log "Checking for requirements ..."

    while IFS= read -r TOOL
    do
        log "Looking for ${TOOL} ..."

        command -v "${TOOL}" &> /dev/null
        __LOCAL_EXIT_STATUS=$?

        if [ ${__LOCAL_EXIT_STATUS} -ne 0 ]
        then
            log  "*** ${TOOL} is not installed. Please install ${TOOL} to run Social Network."
            break
        fi

    done < "${REQUIREMENTS}"

    eval $__RESULT="'${__LOCAL_EXIT_STATUS}'"
}

# Verify that the required paths are in place
function create_base_path() {
    local __RESULT=$1
    local __LOCAL_EXIT_STATUS=0

    log "Checking for base path ..."

    if [ -d "${NETWORKS_HOME}" ]
    then
        log "The ${NETOWKRS_HOME} directory already exists."
    else
        mkdir "${NETWORKS_HOME}" &> /dev/null
        __LOCAL_EXIT_STATUS=$?
    fi

    eval ${__RESULT}="'${__LOCAL_EXIT_STATUS}'"
}

# Create the required paths inside the network
# data directory
function create_required_paths() {
    local __NETNAME=$1
    local __RESULT=$2
    local __LOCAL_EXIT_STATUS=0
    
    while IFS= read -r DIR
    do
        read -ra ARRAY <<< "${DIR}"

        if [ "${ARRAY[0]}" == "dir" ]
        then
            DIR_TO_CREATE="${NETWORKS_HOME}/${__NETNAME}/${ARRAY[1]}"
        
            log "Creating ${DIR_TO_CREATE} ..."
            mkdir -p "${DIR_TO_CREATE}" &> /dev/null
            __LOCAL_EXIT_STATUS=$?

            if [ "${__LOCAL_EXIT_STATUS}" -ne 0 ]
            then
                log  "*** Can't create the ${DIR_TO_CREATE} required path."
                break
            fi
        else
            if touch "${NETWORKS_HOME}/${__NETNAME}/${ARRAY[1]}" &> /dev/null ;
            then
                log "${NETWORKS_HOME}/${__NETNAME}/${ARRAY[1]} created."
            else
                log "*** Can't create ${NETWORKS_HOME}/${__NETNAME}/${ARRAY[1]}"
                __LOCAL_EXIT_STATUS=1
                break
            fi
        fi
        
    done < "${REQUIRED_PATHS}"
    
    eval ${__RESULT}="'${__LOCAL_EXIT_STATUS}'"
}
