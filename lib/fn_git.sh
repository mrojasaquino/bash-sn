#!/bin/bash
# Function definitions for git related functionality

# Create a new social network
function create_network() {
    local __NETNAME=$1
    local __RESULT=$2
    local __LOCAL_EXIT_STATUS=0

    # check if the nete already exists
    if [ -d "${NETWORKS_HOME}/${__NETNAME}" ]
    then
        echo "*** The network already exists."
        __LOCAL_EXIT_STATUS=1
    else
        if mkdir "${NETWORKS_HOME}/${__NETNAME}" && cd "${NETWORKS_HOME}/${__NETNAME}" ;
        then
            create_required_paths "${__NETNAME}" STATUS
            
            if [ "${STATUS}" -eq 0 ]
            then
                git init
                git add --all
                git config user.email "${USER_EMAIL}"
                git config user.name "${USER_NAME}"
                git commit -m "First commit. Social Network ${__NETNAME} initialized."
                git remote add origin "${SN_GIT_USER}@${SN_GIT_HOST}:${CENTRAL_REPO_PATH}/${__NETNAME}.git"
                git push origin master

                log "The ${__NETNAME} network has been created."
            else
                log "*** Unable to create the required paths."
                __LOCAL_EXIT_STATUS=1
            fi
        else
            log "*** Unable to create the new social network directory."
            __LOCAL_EXIT_STATUS=1
        fi
    fi

    eval ${__RESULT}="'${__LOCAL_EXIT_STATUS}'"
}


# Commit post(s)
function commit() {
    local __NETNAME=$1
    local __RESULT=$2
    local __LOCAL_EXIT_STATUS=0

    if cd "${NETWORKS_HOME}/${__NETNAME}" ;
    then
        git add .
        git commit -am "Updating social network."
    else
        log "*** Can't enter into the ${NETWORKS_HOME}/${__NETNAME} directory."
        __LOCAL_EXIT_STATUS=1
    fi

    eval ${__RESULT}="'${__LOCAL_EXIT_STATUS}'"
}

# Show author's name and email addresses
function show_authors() {
    local __NETNAME=$1
    local __RESULT=$2
    local __LOCAL_EXIT_STATUS=0

    if cd "${NETWORKS_HOME}/${__NETNAME}" ;
    then
        while IFS= read -r AUTHOR
        do
            NAME="${AUTHOR%:*}"
            EMAIL="${AUTHOR##*:}"
            PREFIX=""

            [ "${EMAIL}" == "${USER_EMAIL}" ] && PREFIX="* "

            echo "${PREFIX}${NAME} -> ${EMAIL}"

        done < <(git log --pretty=format:"%an:%ae" | sort -u)
    else
        log "*** Can't enter into the ${NETWORKS_HOME}/${__NETNAME} directory."
        __LOCAL_EXIT_STATUS=1
    fi

    eval ${__RESULT}="'${__LOCAL_EXIT_STATUS}'"
}

# Join an already existing social network from the central server
function join_network() {
    local __NETNAME=$1
    local __RESULT=$2
    local __LOCAL_EXIT_STATUS=0

    local NETWORK_FULL_PATH="${SN_HOME}/${__NETNAME}"

    # Check if we already joined
    if [ -d "${NETWORK_FULL_PATH}" ]
    then
        echo "You are already a member of the ${__NETNAME} network."
    else
        if cd "${NETWORKS_HOME}"
        then
            git clone "${SN_GIT_USER}@${SN_GIT_HOST}:${CENTRAL_REPO_PATH}/${__NETNAME}.git"
            if cd "${NETWORKS_HOME}/${__NETNAME}"
            then
                git config user.email "${USER_EMAIL}"
                git config user.name "${USER_NAME}"
            else
                log "*** Can't enter into the "
            fi

        else
            log "*** Can't enter into the ${SN_HOME} directory."
        fi
    fi


    eval ${__RESULT}="'${__LOCAL_EXIT_STATUS}'"
}


# Pull updates to the social network from the central server
function pull_updates() {
    local __NETNAME=$1
    local __RESULT=$2
    local __LOCAL_EXIT_STATUS=0   

    if cd "${NETWORKS_HOME}/${__NETNAME}"
    then
        git pull origin master
        __LOCAL_EXIT_STATUS=$?
    else
        log "*** Can't enter into the ${NETWORKS_HOME}/${__NETNAME} directory"
        __LOCAL_EXIT_STATUS=1
    fi

    eval ${__RESULT}="'${__LOCAL_EXIT_STATUS}'"
}

# Push updates to the social network to the central server
function push_changes() {
    local __NETNAME=$1
    local __RESULT=$2
    local __LOCAL_EXIT_STATUS=0   

    if cd "${NETWORKS_HOME}/${__NETNAME}"
    then
        git push origin master
        __LOCAL_EXIT_STATUS=$?
    else
        log "*** Can't enter into the ${NETWORKS_HOME}/${__NETNAME} directory"
        __LOCAL_EXIT_STATUS=1
    fi

    eval ${__RESULT}="'${__LOCAL_EXIT_STATUS}'"
}