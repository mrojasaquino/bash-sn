#!/bin/bash


function split_elements() {
    while IFS= read -r ELEMENT
    do
        #TYPE=$(echo "${ELEMENT}" | cut -d' ' -f1)
        #PATH=$(echo "${ELEMENT}" | cut -d' ' -f2)

        read -ra ARR <<< "${ELEMENT}"


        echo "Element ${ARR[1]} is of type ${ARR[0]}"
    done < ../conf/network_paths.txt
}


function create_post() {
    vi /tmp/post 

    if [ -e /tmp/post ] && [ -s /tmp/post ]
    then
        echo "The post is not empty."
    fi
}

create_post