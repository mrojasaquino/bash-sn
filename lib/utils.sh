#!/bin/bash

# Various utility functions

# Log to file
function log() {
    echo "$(date +'%Y-%m-%d %H:%M:%S')  $1" >> "${LOG_FILE}"
}


# Show usage to the end user
function show_usage() {
         #         1         2         3         4         5         6         7         8
         #12345678901234567890123456789012345678901234567890123456789012345678901234567890
    echo "Usage : sn OPTION NETWORK_NAME"
    echo " "
    echo "  create NETWORK_NAME          Create a new social network named NETWORK_NAME"
    echo "  join NETWORK_NAME            Join the NETWORK_NAME network"
    echo "  pull NETWORK_NAME            Download new posts for the NETWORK_NAME network"
    echo "  log NETWORK_NAME             Show existing posts in NETWORK_NAME"
    echo "  show NETWORK_NAME {POST_ID | following | followers }"
    echo "                               Show the post POST_ID from the NETWORK_NAME "
    echo "                               network, or the authors you're following to, or"
    echo "                               the users that are following you."
    echo "  post NETWORK_NAME            Create a new post for the NETORK_NAME"
    echo "  like NETWORK_NAME POST_ID    Give a like to the post POST_ID in NETWORK_NAME"
    echo "  push NETWORK_NAME            Send local changes in NETWORK_NAME to the "
    echo "                               central server"
    echo "  members NETWORK_NAME         Show the network's members"
    echo "  follow NETWORK_NAME MEMBER   Follow the posts of the member of a social"
    echo "                               network member. You must enter the author "
    echo "                               email"
    echo "  unfollow NETWORK_NAME MEMBER Unfollow member from NETWORK_NAME"
    echo " "
}