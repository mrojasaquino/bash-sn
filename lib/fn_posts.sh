#!/bin/bash

# Posts related functions

# Create a new post
function create_post() {
    local __NETNAME=$1
    local __RESULT=$2
    local __LOCAL_EXIT_STATUS=0

    local POST_PATH
    local POST_ID

    POST_PATH="${NETWORKS_HOME}/${__NETNAME}/posts/$(date +%Y/%m/%d)"
    POST_ID=$(uuidgen)

    if [ ! -d "${POST_PATH}" ]
    then
        log "Creando ${POST_PATH}"
        mkdir -p "${POST_PATH}"
    fi

    local POST_FILE="${POST_PATH}/${POST_ID}"

    vi "${POST_FILE}"

    if [ -e "${POST_FILE}" ] && [ -s "${POST_FILE}" ]
    then
        TITLE=$(head -1 "${POST_FILE}" | tr -c '[:alnum:]' '_')

        # Add post footer
        { 
            echo " "
            echo "--"
            echo "Author: ${USER_NAME} | ${USER_EMAIL} | $(date +'%Y-%m-%d %H:%M')"
            echo " " 
        } >> "${POST_FILE}"

        mv "${POST_FILE}" "${POST_PATH}/${TITLE}-${POST_ID}.txt"

        echo "The post has been created."
        __LOCAL_EXIT_STATUS=0
    else
        log "*** The post file ${POST_FILE} doesn't exists"
        __LOCAL_EXIT_STATUS=1
    fi

    eval ${__RESULT}="'${__LOCAL_EXIT_STATUS}'"
}


# Shows the existing posts
function list_posts() {
    local __NETNAME=$1
    local __RESULT=$2
    local __LOCAL_EXIT_STATUS=0

    local TITLE
    local FILE_NAME
    local _LIKES

    if cd "${NETWORKS_HOME}/${__NETNAME}" ;
    then
        echo " "
        # Ignore the dummy file. It is added initialy so git makes no complains
        for POST_FILE in $(git log --pretty="" --name-only posts | grep -v dummy)
        do

            _LIKES=""
            count_likes "${NETWORKS_HOME}/${__NETNAME}/${POST_FILE}" _LIKES

            TITLE=$(grep Author "${POST_FILE}")
            FILE_NAME=" - ${POST_FILE##*/}"

            echo " - ${TITLE} => ${FILE_NAME} | Likes ${_LIKES}"

        done

        echo " "

    fi

    eval ${__RESULT}="'${__LOCAL_EXIT_STATUS}'"
}


# counts the likes for an existing post, it expects the full
# post path
function count_likes() {
    local __POST=$1
    local __RESULT=$2
    local __LIKES=0
    local __LIKES_FILE

    if [ -e "${__POST}" ]
    then
        # Replace "posts" with "likes" in the path
        __LIKES_FILE="${__POST/posts/likes}"
        
        if [ -e "${__LIKES_FILE}" ] 
        then
            __LIKES=$(wc -l "${__LIKES_FILE}" | cut -d' ' -f1)
        fi

    fi

    eval ${__RESULT}="'${__LIKES}'"
}


# Shows a specific post
function show_post() {
    local __NETNAME=$1
    local __POST_ID=$2
    local __RESULT=$3
    local __LOCAL_EXIT_STATUS=0   

    local POST_FILE

    if cd "${NETWORKS_HOME}/${__NETNAME}"
    then
        POST_FILE=$(find posts -type f -name "${__POST_ID}")

        if [ "${POST_FILE}" == "" ]
        then
            echo "*** The post doesn't exists in the network."      
        else
            less "${POST_FILE}"
        fi      
        __LOCAL_EXIT_STATUS=1
    fi


    eval ${__RESULT}="'${__LOCAL_EXIT_STATUS}'"
}


# Gives like to a specific post
function like_post() {
    local __NETNAME=$1
    local __POST_ID=$2
    local __RESULT=$3
    local __LOCAL_EXIT_STATUS=1

    local POST_PATH
    local LIKES_PATH
    local LIKES=0
    local POST_FILE
    local FILE_NAME


    if cd "${NETWORKS_HOME}/${__NETNAME}/posts"
    then
        POST_FILE="$(find . -type f -name "${__POST_ID}")"

        if [ "${POST_FILE}" != "" ]
        then
            POST_PATH="${POST_FILE%/*}"

            LIKES_PATH="${NETWORKS_HOME}/${__NETNAME}/likes/${POST_PATH}"

            if [ ! -d "${LIKES_PATH}" ]
            then
                log "*** Creating likes directory ${LIKES_PATH} "
                mkdir -p "${LIKES_PATH}"
            fi

            LIKES_PATH="${LIKES_PATH}/${__POST_ID}"

            if [ -e "${LIKES_PATH}" ]
            then
                LIKES=$(grep -cE "${USER_EMAIL}" "${LIKES_PATH}")
            fi
            
            if [ "${LIKES}" -eq 0 ]
            then
                echo "${USER_EMAIL}" >> "${LIKES_PATH}"
            else
                echo "You already liked this post."
            fi

            __LOCAL_EXIT_STATUS=0
        else
            echo "*** The post file ${POST_FILE} doesn't exists."
        fi
    else
        echo "*** The ${NETWORKS_HOME}/${__NETNAME}/posts path doesn't exists."
    fi

    eval ${__RESULT}="'${__LOCAL_EXIT_STATUS}'"
}


# Follow a user
function follow_member() {
    local __NETNAME=$1
    local __MEMBER=$2
    local __RESULT=$3
    local __LOCAL_EXIT_STATUS=0

    local FOLLOWS_FILE="${NETWORKS_HOME}/${__NETNAME}/follows/${USER_EMAIL}.txt"

    if [ "${USER_EMAIL}" == "${__MEMBER}" ]
    then
        echo "*** You can't follow yourself."
        __LOCAL_EXIT_STATUS=1       
    else

        if [ -e "${FOLLOWS_FILE}" ]
        then

            FOLLOWING=$(grep -cE "${__MEMBER}" "${FOLLOWS_FILE}")

            if [ "${FOLLOWING}" == "0" ]
            then
                echo "${__MEMBER}" >> "${FOLLOWS_FILE}"
            fi
        else
            echo "${__MEMBER}" >> "${FOLLOWS_FILE}"
        fi
    fi

    eval ${__RESULT}="'${__LOCAL_EXIT_STATUS}'"
}


# Unfollow a user
function unfollow_member() {
    local __NETNAME=$1
    local __MEMBER=$2
    local __RESULT=$3
    local __LOCAL_EXIT_STATUS=0

    local FOLLOWS_FILE="${NETWORKS_HOME}/${__NETNAME}/follows/${USER_EMAIL}.txt"

    if [ -e "${FOLLOWS_FILE}" ]
    then
        # check if I'm really following to that member
        FOLLOWING=$(sed -ne "/${__MEMBER}/p" "${FOLLOWS_FILE}")

        if [ "${FOLLOWING}" == "" ]
        then
            echo "You aren't following to the member ${__MEMBER}."
        else
            sed -i "/${__MEMBER}/d" "${FOLLOWS_FILE}"
            __LOCAL_EXIT_STATUS=$?
        fi
    fi


    eval ${__RESULT}="'${__LOCAL_EXIT_STATUS}'"
}


# Show authors you are following to
function show_following() {
    local __NETNAME=$1
    local __LOCAL_EXIT_STATUS=0
    local __RESULT=$2

    local __FOLLOWS_FILE="${NETWORKS_HOME}/${__NETNAME}/follows/${USER_EMAIL}.txt"

    echo " "

    if [ -e "${__FOLLOWS_FILE}" ]
    then
        cat "${__FOLLOWS_FILE}"
    else
        echo "*** You're following to no authors."
    fi

    echo " "

    eval ${__RESULT}="'${__LOCAL_EXIT_STATUS}'"
}


# Show authors following you
function show_followers() {
    local __NETNAME=$1
    local __RESULT=$2
    local __LOCAL_EXIT_STATUS=0

    if cd "${NETWORKS_HOME}/${__NETNAME}/follows" 
    then
        grep -l "${USER_EMAIL}" -- *.txt | sed 's/^/\ - \ /g; s/\.txt//g'
        echo " "
    fi

    eval ${__RESULT}="'${__LOCAL_EXIT_STATUS}'"
}