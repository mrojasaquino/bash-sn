# Developer Reference

This is the Social Network developer's reference. Here you'll find all the technical data in order to understand the structure and requirements for the Social Netork system to work.

## External Requirements
The Social Netowrk application is conformed from BASH scripts that contains the business logic. Those scripts make use of commonly used Linux / Unix command line utilities.

Those programs are listed in the conf requirements and are validated each time the social netowrk program is run. Those are extrnal commands that are no part of the BASH builtins, hence the need to make sure that those programs are already installed. In case you wish to add a new functionality that depends on an external command, it's a good idea to add said command in this file.

Currently, the system depends on:

* `sed`
* `git`
* `uuidgen`
* `vi`
* `grep`


## Required Paths

The system requires some paths to be available in each social network in order to be usable. This paths are defined at the `conf/network_paths.txt`:

* `dir posts` : The directory to hold all the posts. Here a new subdirectories structure is created in order to group the posts in a year/month/day structure.
* `dir likes` : A directory to keep a reference to all the likes a post gets. Here, each post will have a file containing the user emails of each user that gave a like to a post.
* `dir follows` : A directory to keep references betwwen users following authors. Here, each author will have a file enlisting all of her/his followers.
* `file posts/dummy` : a dummy file, just to keep git from complaining when there are still no posts.
* `file likes/dummy` : a dummy file, just to keep git from complaining when there are still no likes to posts.
* `file follows/dummy` : a dummy file, just to keep git from complaining when there are still no author / followers relationships.

If you look closely, each line defines an element type and name, so the line `dir posts` defines that a directory named `posts` should exist for each social network. The line `posts/dummy` dictates that a file named `dummy` should be created inside the `posts` directory.

All this elements are used when a new social network is created. So, if you add a new function that requires some directories and/or files to exist inside a social network, you should add them here so they are built when adding a new social network.


## System Configuration

The user system configuration is available in the `conf.sh`. Here, we define the following:

* `SN_HOME` : The system base directory. Here is where the application is installed.
* `CENTRAL_REPO_PATH` : The base path in the Git server where all the social networks' repositories are.
* `SN_GIT_USER` : The git repository user name.
* `SN_GIT_HOST` : The git server host name or IP address.
* `USER_NAME` : The full user name. This is used as the git committer / author name as well as the post author name.
* `USER_EMAIL` : The user email. This is used as the git commiter / author email as well as a the post author email.
* `NETWORKS_HOME` : The data directory for the social networks. Here a directory is created for each repository / social network. Those directories contains the file structure detailed in the [Required Paths](#required-paths) section.
* `REQUIREMENTS` : The path where the required utilities file is located.
* `REQUIRED_PATHS` : The path where the required paths file is located.
* `LOG_FILE` : The path where the log file is created.



## System Files

The Social Network system is built as a modular system that can be expanded or modified without breaking the user interface. You could, for example, you could use a database instead of Git, and the user will not find this. Even you could siwtch from one storage engine to the other, based on some aditional configurations. So to follow this principle, it is comprised of the following main files.

### `sn`
The main program, it's located directly in the `SocialNetwork` directory. This is the one the user will invoke from the command line. Basically, it just checks the comand line arguments to see if the user passed at least two (an action command and a network name) and to invoke the corresponding function. On every invocation, it runs an initialization function that checks that the required programs are in place and if the local data directory exists.


### `conf.sh`
System configuration file. It contains the required environment variables, as described in the [System Configuration](#system-configuration).


### `lib/fn_git.sh`
Function definitions related to the Git tool. We're using Git to keep track of all the posts, and as a centralized repository for all the social networks.


### `lib/fn_posts.sh`
Function definitios related to post handling. 


### `lib/fn_preflight.sh`
Pre flight checks. Functions used to check that the required utilities and paths exists before trying to create any post. Those functions are invoked each time the `sn` program is run.


### `lib/utils.sh`
Some utility functions such as logging and usage showing.


## Git as a Post Repository

As we've already mentioned, Git is being used as the repository for all the social network data storage. Some functions, such as the push and pull operations, require connection to a central repository, so the user is required to have access to said repository, and that the specific social network repository exists in the server. So, at least at this time, before creating a new social network, the user needs to create, manually or by asking to the repository administrator, a bare git repository at the central server, as it will be added as a remote origin to the local social network repository.


## Social Network Files

The current data sotrage for the social network software is text file based. As such, it can be processed using standard command line tools.

### Posts
The subdirectory `posts` contains all the posts for the social network, for all the users. Under this subdirectory, a tree structure is created to store all the posts in a year/month/date structure, so, besides having a unique identifier, all the posts are stored by their creation date.

The file name for each post is comprised of the first line of the post (the title), with all non numeric characters converted to underscores(\_), plus a UUID. Inside each post, the system adds the full user name, the user email, and the date and time of the post.


### Likes
Inside the `likes` subdirectory we have a structure similar to that of the `posts` but each file contains the email of the user who gave a like to each post. When the user request the list of all the posts, the number of likes for the post is added to the en of the line of each listed post.


### Follow
In the `follows` directory, a file is created for each member of the netowrk who requests to follow a user, and inside each file is the list of the users that the user is following.