# Social Network Application

Welcome to the command line Social Network application. It's a software system wrriten mainly in BASH, using several command line utilities commonly found in Linux / Unix systems:

* sed
* git
* uuidgen
* vi
* grep

The main program is named `sn` and is in the same path as this readme file.


## Configuration
The `lib/conf.sh` fils defines some environment variables required for the system to work:

* `SN_HOME`: the full path to the installation directory of the Social Network application. It should be adapted to your particular system.
* `CENTRAL_REPO_PATH`: the Git central repository base path for all the social networks you are using.
* `SN_GIT_USER`: Your Git user in the Social Network's central server.
* `SN_GIT_HOST`: The Git server name or IP address.
* `USER_NAME` : The git full user name. It's used in the posts body as the author name.
* `USER_EMAIL`: The git user email. It's used un the posts besides the user full name.
* `NETWORKS_HOME`: The directory where the social networks' data files would be located.


## Options
The following operations are available in the Social Network application:

* `sn create`: Create a new social network. Plase, take into account that in order to synchronize your networks with the central repository, you should ask to  the repository admin to create a new git repository with the same name as the social network that you're creating
* `sn join`: Join an existing social network by cloning it to your PC. Plase take into account that the network that you0re trying to join must previously exist in the central repository.
* `sn pull`: Pull in new posts and likes
* `sn log`: Show a list of existing posts
* `sn show`: Show a specified post, the authors that you're following to, or the users that follows you
* `sn post`: Post a new story
* `sn like`: Like a specified post
* `sn push`: Push locally made changes back to the server
* `sn members`: Show the network's members
* `sn follow`: Follow the posts of the specified member
* `sn unfollow`: Unfollow the posts of the specified member

Each operation requires at least the social network name to be provided. For example, if you wan to create a new social network, you should enter in your command prompt:

```bash
./sn create TheProgrammersNetwork
```

where `TheProgrammersNetwork` is the name of the social network you're creating.

If you want to create a new post:

```bash
./sn post TheProgrammersNetwork
```

Some options require to enter a third parameter:

```bash
./sn like TheProgrammersNetowrk A_Lonely_Developer_Rant-ddee4551-079d-4e06-aeca-41ea67285e31.txt
```

Here, you're giving a like to the post `A_Lonely_Developer_Rant-ddee4551-079d-4e06-aeca-41ea67285e31.txt` in the `TheProgrammersNetowrk` social netowrk.

Another example are the options to show the authors you're following to

```bash
./sn show TheProgrammersNetwork following
```

or the users that are following your posts

```bash
./sn show TheProgrammersNetwork followers
```


## Getting help
You can see all the operations available in the social network application by entering `./sn help` or `./sn` in the command prompt.

For additional info or bugs reports, please raise an issue in the [project's repository](https://gitlab.com/mrojasaquino/bash-sn).